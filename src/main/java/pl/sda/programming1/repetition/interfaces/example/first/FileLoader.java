package pl.sda.programming1.repetition.interfaces.example.first;

interface FileLoader {

    void loadFile();
}
