package pl.sda.programming1.repetition.interfaces.definition;

//multiple inheritance on interfaces
interface InterfaceExample extends FirstPreInterface, SecondPreInterface {

    //----------------------------Standard Method --------------------------------------------------------
    public abstract void methodToImplement();

    //public and abstract in above method not required (added implicit here)
    void methodToImplement2();

    //----------------------------- Constants -----------------------------------------------------

    public static final String CONSTANT = "Constant value";

    //public, static and final in above constant not required (added implicit here)
    String CONSTANT_2 = "Constant value";


    //------------------------------ Default Method ------------------------------------------------

    default String defaultMethod() {
        return "Default implementation";
    }

    //------------------------------ Static Method ------------------------------------------------

    static void staticMethod() {
        System.out.println("Inside static method");
    }

    //------------------------------ Private Method ------------------------------------------------
    private void privateMethod() {
        System.out.println("Inside private method");
    }

    private static void privateStaticMethod() {
        System.out.println("Inside private static method");
    }
}
