package pl.sda.programming1.repetition.interfaces.example.first;

class LaserPrinter implements Printer {
    @Override
    public void print() {
        System.out.println("I'm printing with laser");
    }
}
