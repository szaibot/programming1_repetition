package pl.sda.programming1.repetition.interfaces.definition;

class InterfaceImpl implements InterfaceExample, SecondInterfaceExample {
    @Override
    public void methodToImplement() {

    }

    @Override
    public void methodToImplement2() {

    }

    @Override
    public void firstPreMethod() {

    }

    @Override
    public void secondInterfaceMethod() {

    }

    @Override
    public void secondPreMethod() {

    }
}
