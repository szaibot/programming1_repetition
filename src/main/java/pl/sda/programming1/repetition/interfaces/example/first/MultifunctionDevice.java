package pl.sda.programming1.repetition.interfaces.example.first;

class MultifunctionDevice implements Copier, Printer, Scanner, FileLoader {
    @Override
    public void copy() {
        System.out.println("I'm copying");
    }

    @Override
    public void print() {
        System.out.println("I'm printing");
    }

    @Override
    public void scan() {
        System.out.println("I'm scanning");
    }

    @Override
    public void loadFile() {
        System.out.println("I'm loading file from phone");
    }
}
