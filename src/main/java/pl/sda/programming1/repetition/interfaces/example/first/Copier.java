package pl.sda.programming1.repetition.interfaces.example.first;

interface Copier {

    void copy();
}
