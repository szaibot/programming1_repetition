package pl.sda.programming1.repetition.abstraction.first;

abstract class Quadrangle extends Shape {

    protected final double longerEdgeLength;
    protected final double shorterEdgeLength;

    Quadrangle(boolean regular, double longerEdgeLength, double shorterEdgeLength) {
        super(4, regular);
        this.longerEdgeLength = longerEdgeLength;
        this.shorterEdgeLength = shorterEdgeLength;
    }
}
