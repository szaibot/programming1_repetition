package pl.sda.programming1.repetition.abstraction.first;

class Rectangle extends Quadrangle implements PerimeterCalculator {

    Rectangle(double longerEdgeLength, double shorterEdgeLength) {
        super(false, longerEdgeLength, shorterEdgeLength);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "longerEdgeLength=" + longerEdgeLength +
                ", shorterEdgeLength=" + shorterEdgeLength +
                ", numberOfEdges=" + numberOfEdges +
                ", regular=" + regular +
                "} ";
    }

    double calculateArea() {
        return longerEdgeLength * shorterEdgeLength;
    }

    @Override
    public double calculatePerimeter() {
        return (longerEdgeLength * 2) + (shorterEdgeLength + 2);
    }
}
