package pl.sda.programming1.repetition.abstraction.second;

import java.util.ArrayList;
import java.util.List;

class Main {

    public static void main(String[] args) {
        Human human = new Human(1, Gender.MALE);
        Lion lion = new Lion(2, Gender.FEMALE);
        Frog frog = new Frog(3);
        Salmon salmon = new Salmon(4);

        List<Animal> animals = new ArrayList<>();

        animals.add(human);
        animals.add(lion);
        animals.add(frog);
        animals.add(salmon);

        for (Animal animal : animals) {
            System.out.println("Animal:" + animal.getClass().getSimpleName());
            animal.breathe();
        }

        animals.forEach(animal -> {
            System.out.println("Animal:" + animal.getClass().getSimpleName());
            animal.breathe();
        });

        Lion secondLion = new Lion(5, Gender.FEMALE);
        Human secondHuman = new Human(3, Gender.FEMALE);

        secondHuman.feedWithBreast();
        secondLion.feedWithBreast();

        Animal kingLion = new Lion(1, Gender.FEMALE);
        //kingLion.feedWithBreast(); compilation error, why?
        //proper solution:
        Mammal newKingLion = new Lion(1, Gender.MALE);
        newKingLion.feedWithBreast();



    }
}
