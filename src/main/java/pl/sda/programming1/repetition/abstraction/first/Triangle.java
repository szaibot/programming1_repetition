package pl.sda.programming1.repetition.abstraction.first;

class Triangle extends Shape implements PerimeterCalculator {
    private final TriangleType type;
    private final double aEdgeLength;
    private final double bEdgeLength;
    private final double cEdgeLength;

    Triangle(boolean regular, TriangleType type, double aEdgeLength, double bEdgeLength, double cEdgeLength) {
        super(3, regular);
        this.type = type;
        this.aEdgeLength = aEdgeLength;
        this.bEdgeLength = bEdgeLength;
        this.cEdgeLength = cEdgeLength;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "type=" + type +
                ", aEdgeLength=" + aEdgeLength +
                ", bEdgeLength=" + bEdgeLength +
                ", cEdgeLength=" + cEdgeLength +
                ", numberOfEdges=" + numberOfEdges +
                ", regular=" + regular +
                "} " + super.toString();
    }

    double calculateArea() {
        double halfPerimeter = (aEdgeLength + bEdgeLength + cEdgeLength)/2;
        double underSquare = halfPerimeter * (halfPerimeter - aEdgeLength) * (halfPerimeter - bEdgeLength) * (halfPerimeter - cEdgeLength);
        return Math.sqrt(underSquare);
    }

    @Override
    public double calculatePerimeter() {
        return aEdgeLength + bEdgeLength + cEdgeLength;
    }
}
