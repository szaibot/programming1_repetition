package pl.sda.programming1.repetition.abstraction.second;

class Human extends Mammal {
    Human(int age, Gender gender) {
        super(age, gender);
    }
}
