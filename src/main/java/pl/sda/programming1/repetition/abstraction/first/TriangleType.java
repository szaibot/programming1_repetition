package pl.sda.programming1.repetition.abstraction.first;

enum TriangleType {
    EQUILATERAL,
    ISOSCELES,
    SCALENE
}
