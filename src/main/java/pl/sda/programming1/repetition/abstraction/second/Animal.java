package pl.sda.programming1.repetition.abstraction.second;

abstract class Animal {

    protected final int age;

    protected Animal(int age) {
        this.age = age;
    }

    abstract String getBreathingOrgan();

    final void breathe() {
        System.out.println("I'm breathing with " + getBreathingOrgan() + "!");
    }
}
