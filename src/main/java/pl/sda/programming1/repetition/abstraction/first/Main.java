package pl.sda.programming1.repetition.abstraction.first;

import java.util.ArrayList;
import java.util.List;

class Main {

    public static void main(String[] args) {
        Triangle triangle = new Triangle(false, TriangleType.SCALENE, 1, 2, 3);
        System.out.println(triangle);

        Triangle firstTriangle = new Triangle(true, TriangleType.EQUILATERAL, 3, 3, 3);
        Triangle secondTriangle = new Triangle(false, TriangleType.ISOSCELES, 3, 3, 5);

        Rectangle firstRectangle = new Rectangle(2, 3);
        Rectangle secondRectangle = new Rectangle(5, 10);

        Square firstSquare = new Square(3);
        Square secondSquare = new Square(10);

        System.out.println("----------------Triangles");
        System.out.println(firstTriangle);
        System.out.println(secondTriangle);

        System.out.println("----------------Rectangles");
        System.out.println(firstRectangle);
        System.out.println(secondRectangle);

        System.out.println("----------------Squares");
        System.out.println(firstSquare);
        System.out.println(secondSquare);

        List<Shape> shapes = new ArrayList<Shape>();
        shapes.add(firstTriangle);
        shapes.add(secondTriangle);
        shapes.add(firstRectangle);
        shapes.add(secondRectangle);
        shapes.add(firstSquare);
        shapes.add(secondSquare);

        for (Shape shape : shapes) {
            System.out.println("Shape: " + shape);
            System.out.println("Area: " + shape.calculateArea());
        }
    }
}
