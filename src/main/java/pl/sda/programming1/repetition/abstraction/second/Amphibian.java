package pl.sda.programming1.repetition.abstraction.second;

class Amphibian extends Animal {
    Amphibian(int age) {
        super(age);
    }

    @Override
    String getBreathingOrgan() {
        return "gills, lungs and skin";
    }
}
