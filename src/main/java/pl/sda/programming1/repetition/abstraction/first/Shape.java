package pl.sda.programming1.repetition.abstraction.first;

abstract class Shape {
    protected int numberOfEdges;
    protected boolean regular;

    Shape(int numberOfEdges, boolean regular) {
        this.numberOfEdges = numberOfEdges;
        this.regular = regular;
    }

    abstract double calculateArea();
}
