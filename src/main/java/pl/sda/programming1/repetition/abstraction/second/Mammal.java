package pl.sda.programming1.repetition.abstraction.second;

class Mammal extends Animal {
    private final Gender gender;

    Mammal(int age, Gender gender) {
        super(age);
        this.gender = gender;
    }

    @Override
    String getBreathingOrgan() {
        return "lungs";
    }

    void feedWithBreast() {
        if (gender == Gender.MALE) {
            throw new IllegalArgumentException("Male cannot feed with breast");
        }

        System.out.println("I’m feeding with breast");
    }
}
