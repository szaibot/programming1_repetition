package pl.sda.programming1.repetition.abstraction.first;

class Square extends Quadrangle implements PerimeterCalculator {
    Square(double edgeLength) {
        super(true, edgeLength, edgeLength);
    }

    @Override
    public String toString() {
        return "Square{" +
                "edgeLength=" + longerEdgeLength +
                ", shorterEdgeLength=" + shorterEdgeLength +
                ", numberOfEdges=" + numberOfEdges +
                ", regular=" + regular +
                "} ";
    }

    double calculateArea() {
        return longerEdgeLength * shorterEdgeLength;
    }

    @Override
    public double calculatePerimeter() {
        return (longerEdgeLength * 2) + (shorterEdgeLength + 2);
    }
}
