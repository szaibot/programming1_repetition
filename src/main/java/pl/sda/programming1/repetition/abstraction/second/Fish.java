package pl.sda.programming1.repetition.abstraction.second;

class Fish extends Animal {
    Fish(int age) {
        super(age);
    }

    @Override
    String getBreathingOrgan() {
        return "gills";
    }
}
