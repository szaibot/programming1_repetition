package pl.sda.programming1.repetition.abstraction.second;

class Lion extends Mammal {
    Lion(int age, Gender gender) {
        super(age, gender);
    }
}
