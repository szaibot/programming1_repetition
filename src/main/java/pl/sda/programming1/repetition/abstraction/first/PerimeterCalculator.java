package pl.sda.programming1.repetition.abstraction.first;

interface PerimeterCalculator {

    double calculatePerimeter();
}
